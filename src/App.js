import './App.css';
import Main from './pages/main/main';
import Navbar from './pages/main/Navbar';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Main />
    </div>
  );
}

export default App;
