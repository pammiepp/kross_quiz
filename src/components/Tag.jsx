import { Tag } from 'antd'
import React from 'react'

function TagMember({ children, color }) {

    return (
        <Tag
            color={color || 'default'}
            className='tag-project-list'
        >
            {children}
        </Tag>
    )
}

export default TagMember