export const Icon = ({ name, size }) => {
    return (
        <span
            className="material-symbols-outlined"
            style={{
                fontSize: size || '18px'
            }}
        >
            {name}
        </span>
    )
}
