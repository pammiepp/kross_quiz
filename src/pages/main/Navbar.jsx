import { Avatar, Badge, Col, Dropdown, Input, Layout, Row } from 'antd';
import React from 'react'
import { SearchOutlined, BellOutlined, DownOutlined } from '@ant-design/icons';
import Logo from '../../assets/images/Kross_logo.PNG'

const { Header } = Layout;

function Navbar() {
    const items = [
        {
            key: '1',
            label: 'Sign out',

        },
    ]
    return (
        <Header className='navbar'>
            <Row gutter={[8, 0]} align="middle">
                <Col span={4} align="left">
                    <img src={Logo} alt="logo" className='kross-logo' />
                </Col>
                <Col
                    md={{ span: 8, offset: 2 }}
                    lg={{ span: 6, offset: 7 }}
                    xl={{ span: 5, offset: 10 }}
                    align="right"
                >
                    <Input
                        placeholder="Search project, content or client"
                        prefix={<SearchOutlined />}
                        className='search-input'
                    />
                </Col>
                <Col md={2} lg={1} align="center">
                    <Badge dot>
                        <BellOutlined style={{ fontSize: '18px' }} />
                    </Badge>
                </Col>
                <Col md={8} lg={6} xl={4} align="right">
                    <Dropdown menu={{ items }}>
                        <Row gutter={[16, 0]} align="middle" justify="start">
                            <Col span={5} align="center">
                                <Avatar shape="square" size={32}>KK</Avatar>
                            </Col>
                            <Col span={17} align="left" justify="middle" style={{ lineHeight: '18px' }}>
                                <div className='text-fullname'>Austin Anderson</div>
                                <div className='text-position'>Account Manager</div>
                            </Col>
                            <Col span={2}> <DownOutlined /></Col>
                        </Row>
                    </Dropdown>
                </Col>
            </Row>
        </Header >
    )
}

export default Navbar