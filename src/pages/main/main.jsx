import { Layout } from 'antd'
import React from 'react'
import SideMenu from './SideMenu'
import ManageProject from '../project-management/index';
import '../../assets/styles/home.css'

const { Content } = Layout;

function Main() {
    return (
        <div>
            <Layout>

                <SideMenu />

                <Layout className='content-layout'>
                    <Content
                        style={{
                            paddingLeft: 64,
                        }}
                    >
                        <ManageProject />

                    </Content>
                </Layout>
            </Layout>

        </div>
    )
}

export default Main