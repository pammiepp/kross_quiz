import { Layout, Menu } from 'antd';
import React from 'react'
import { Icon } from '../../components/Icon';

const { Sider } = Layout;

function SideMenu() {

    const MenuItems = [
        {
            label: <Icon name="work" />,
            key: 'project'
        },
        {
            type: 'divider',
            className: 'divider-sider'
        },
        {
            label: <Icon name="library_books" />,
            key: 'item-2'
        },
        {
            label: <Icon name="redeem" />,
            key: 'item-3'
        },
        {
            label: <Icon name="inventory_2" />,
            key: 'item-4'
        },
        {
            label: <Icon name="share" />,
            key: 'item-5'
        },
        {
            type: 'divider',
            className: 'divider-sider'
        },
        {
            label: <Icon name="leaderboard" />,
            key: 'item-6'
        },
        {
            label: <Icon name="view_list" />,
            key: 'item-7'
        },
        {
            type: 'divider',
            className: 'divider-sider'
        },
        {
            label: <Icon name="person" />,
            key: 'item-8'
        },
        {
            label: <Icon name="settings" />,
            key: 'item-9', className: 'icon-setting'
        },
    ];
    return (
        <Sider width={65} className='home-sider'>
            <Menu
                mode="inline"
                defaultSelectedKeys={['project']}
                className='side-menu'
                items={MenuItems}
            />
        </Sider>
    )
}

export default SideMenu