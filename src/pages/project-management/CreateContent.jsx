import { AppstoreOutlined, CheckSquareOutlined } from '@ant-design/icons'
import { Button, Col, DatePicker, Drawer, Form, Input, Radio, Row, Select } from 'antd'
import projects from '../../data/projects.json'
import React, { useContext, useEffect } from 'react'
import { Icon } from '../../components/Icon';
import { ProjectContext } from '../../context/ProjectContext';
import moment from 'moment';

const { Option } = Select;
function CreateContent({ openDrawer, onClose }) {
    const [form] = Form.useForm();
    const { project } = useContext(ProjectContext)

    useEffect(() => {
        form.setFieldsValue({
            project_name: project.project_name,
            due_date_time: moment(project.due_date),
        })
    }, [project, form])

    const onCloseDrawer = () => {
        form.resetFields()
        onClose()
    }
    return (
        <Drawer
            title={<><AppstoreOutlined /> <span>Create content</span></>}
            placement="right"
            width="650px"
            closable={false}
            onClose={onCloseDrawer}
            open={openDrawer}
            extra={
                <Button type="primary" icon={<CheckSquareOutlined />}>
                    Create content
                </Button>
            }
            forceRender
            className='drawer-content'
        >
            <Form
                layout="vertical"
                name="form"
                form={form}

            >
                <Row gutter={[6, 0]}>
                    <Col span={24}>
                        <Form.Item
                            label="Select project"
                            name="project_name"
                        >
                            <Select
                                allowClear
                            >
                                {projects.map((item, index) => (
                                    <Option key={index} value={item.project_name}>{item.project_name}</Option>
                                ))}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="Topic name"
                            name="topic_name"
                        >
                            <Input />
                        </Form.Item>

                    </Col>

                    <Col span={12}>
                        <Form.Item
                            label="Due date & time"
                            name="due_date_time"
                        >
                            <DatePicker
                                placeholder='Select date & time'
                                showTime={{
                                    format: 'HH:mm',
                                }}
                                format={"DD/MM/YYYY HH:mm"}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item
                            label="Publishing date & time"
                            name="publishing_date_time"
                        >
                            <DatePicker
                                placeholder='Select date & time'
                                showTime={{
                                    format: 'HH:mm',
                                }}
                                format={"DD/MM/YYYY HH:mm"}
                            />
                        </Form.Item>
                    </Col>

                    <Col span={24}>
                        <Form.Item
                            label="Key message"
                            name="key_massage"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Describtion"
                            name="describtion"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Content format"
                            name="content_format"
                        >
                            <Radio.Group className='btn-group-form'>
                                <Radio.Button
                                    value="image"
                                    className='btn-group'
                                >
                                    <Icon name="image" size={16} />
                                    <span style={{ paddingLeft: 12 }}>Image</span>
                                </Radio.Button>
                                <Radio.Button
                                    value="video"
                                    className='btn-group'
                                >
                                    <Icon name="smart_display" size={16} />
                                    <span style={{ paddingLeft: 12 }}>Video</span>
                                </Radio.Button>
                                <Radio.Button
                                    value="writing"
                                    className='btn-group'
                                    style={{ marginRight: 0 }}
                                >
                                    <Icon name="description" size={16} />
                                    <span style={{ paddingLeft: 12 }}>Writing</span>
                                </Radio.Button>
                            </Radio.Group>
                        </Form.Item>

                        <Form.Item
                            label="Post type"
                            name="post_type"
                        >
                            <Radio.Group className='btn-group-col-2 btn-group-form'>
                                <Radio.Button
                                    value="boost post"
                                    className='btn-group'
                                >
                                    <Icon name="arrow_upward" size={16} />
                                    <span style={{ paddingLeft: 12 }}>Boost post</span>
                                </Radio.Button>
                                <Radio.Button
                                    value="non-boost post"
                                    className='btn-group'
                                >
                                    <Icon name="subject" size={16} />
                                    <span style={{ paddingLeft: 12 }}>Non-boost post</span>
                                </Radio.Button>

                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Drawer>
    )
}

export default CreateContent