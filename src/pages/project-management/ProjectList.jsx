import { Col, Divider, List, Row } from 'antd'
import React, { useContext } from 'react'
import '../../assets/styles/projectPage.css'
import { Icon } from '../../components/Icon';
import {
    AppstoreOutlined,
    FacebookFilled,
    TwitterSquareFilled,
    InstagramFilled,
    ShareAltOutlined
} from '@ant-design/icons'
import projects from '../../data/projects.json'
import { ProjectContext } from '../../context/ProjectContext';
import TagMember from '../../components/Tag';

function ProjectList() {
    const { project, setProject } = useContext(ProjectContext)
    const { project_id } = project

    return (
        <List
            itemLayout="vertical"
            dataSource={projects}
            rowKey={(record) => record.id}
            renderItem={(item) => {
                const { assignees } = item

                return <List.Item
                    className={project_id === item.id ? 'selected-list list-project' : 'list-project'}
                    onClick={() => {
                        setProject({
                            ...project,
                            project_id: project_id === item.id ? null : item.id,
                            project_name: project_id === item.id ? "" : item.project_name
                        })
                    }}
                >
                    <Row>
                        <Col span={22} style={{ cursor: 'pointer' }}>
                            <div className='text-project'>{item.project_name}</div>
                            <div className='text-client'>{item.client}</div>
                        </Col>
                        <Col
                            span={2}
                            align="right"
                        >
                            <Icon name="open_in_new" size={14} />
                        </Col>

                    </Row>

                    <Row align="middle" style={{ marginTop: '8px' }}>
                        <Col span={4}>
                            <AppstoreOutlined />
                            <span style={{ marginLeft: '8px' }}>
                                {item.active_event}/{item.all_event}
                            </span>
                        </Col>
                        <Col span={2} align="center">
                            <Divider type="vertical" style={{ height: '14px' }} />
                        </Col>
                        <Col span={12} className='icon-project-share'>
                            <FacebookFilled />
                            <TwitterSquareFilled />
                            <InstagramFilled />
                            <ShareAltOutlined />
                        </Col>
                        <Col span={6} align="right">
                            {assignees.length === 1 ?
                                <TagMember color="#5fcdbc">
                                    {assignees[0].short_name}
                                </TagMember>
                                : <>
                                    <TagMember color="#5fcdbc">
                                        {assignees[0].short_name}
                                    </TagMember>
                                    <TagMember>
                                        +{assignees.length - 1}
                                    </TagMember>
                                </>}

                        </Col>
                    </Row>
                </List.Item>
            }}
        />
    )
}

export default ProjectList