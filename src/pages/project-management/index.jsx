import React, { useState } from 'react'
import { Button, Card, Col, Divider, Radio, Row, Space, Tabs } from 'antd'
import '../../assets/styles/projectPage.css'
import ProjectList from './ProjectList'
import { Icon } from '../../components/Icon'
import { CarryOutOutlined, PlusOutlined } from '@ant-design/icons';
import ProjectCalendar from './ProjectCalendar'
import { ProjectContext } from '../../context/ProjectContext'

function ManageProject() {
    const [tab, setTab] = useState('calendar')
    const [project, setProject] = useState({
        project_id: null,
        project_name: "",
        due_date: null,
    })

    return (
        <ProjectContext.Provider value={{ project, setProject }}>
            <Row style={{ height: '100%' }}>

                <Col span={6} align="left">
                    <Card
                        title={
                            <>
                                <Icon name="work" size={16} />
                                <span style={{ paddingLeft: 12 }}>Project</span>
                            </>
                        }
                        extra={
                            <Button type="primary" icon={<PlusOutlined />}>
                                Create project
                            </Button>}
                        className="card-project-list"
                        size='small'
                    >
                        <Tabs
                            defaultActiveKey="1"
                            tabBarExtraContent={
                                <Space.Compact block className='btn-group-filter'>
                                    <Button icon={<Icon name="filter_alt" />} />
                                    <Button icon={<Icon name="sort" />} />
                                </Space.Compact>
                            }
                            items={[
                                {
                                    label: `Active`,
                                    key: '1',
                                    children: <ProjectList />,
                                },
                                {
                                    label: `Inactive`,
                                    key: '2',
                                    children: `Content of Inactive`,
                                },
                            ]}
                        />
                    </Card>
                </Col>

                <Col span={18}>
                    <Card
                        title={
                            <>
                                <span style={{ paddingLeft: 12 }}>Content</span>
                                <Divider type="vertical" style={{ height: '1.8em' }} />
                                <Radio.Group value={tab} onChange={e => setTab(e.target.value)}>
                                    <Radio.Button
                                        value="calendar"
                                        className='btn-group'
                                    >
                                        <Icon name="calendar_month" size={16} />
                                        <span style={{ paddingLeft: 12 }}>Calendar</span>
                                    </Radio.Button>
                                    <Radio.Button
                                        value="board"
                                        className='btn-group'
                                    >
                                        <Icon name="view_week" size={16} />
                                        <span style={{ paddingLeft: 12 }}>Board</span>
                                    </Radio.Button>
                                </Radio.Group>
                            </>
                        }
                        extra={
                            <Space>
                                <Button type="link" key="Submit" icon={<CarryOutOutlined />}>
                                    Submit for review
                                </Button >
                                <Button type="primary" key="Create" icon={<PlusOutlined />}>
                                    Create content
                                </Button >
                            </Space>
                        }
                        className="card-project-list"
                        size='small'
                    >
                        <ProjectCalendar />

                    </Card >

                </Col>

            </Row>
        </ProjectContext.Provider>
    )
}

export default ManageProject