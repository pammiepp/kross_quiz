import React, { useContext, useEffect, useState } from 'react'
import CreateContent from './CreateContent';
import contentSchedule from '../../data/contentSchedule.json'
import { ProjectContext } from '../../context/ProjectContext';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import '../../assets/styles/calendar.css'


function ProjectCalendar() {
    const [open, setOpen] = useState(false);
    const [schedule, setSchedule] = useState()
    const { project, setProject } = useContext(ProjectContext)


    useEffect(() => {
        const filterSchedule = () => {
            const result = contentSchedule.filter(item => item.id === project.project_id)
            const project_data = project.project_id ? result : contentSchedule

            const schedule = []

            project_data?.map(project => (
                project.content?.map(item => (
                    schedule.push({
                        title: project.project_name,
                        start: item.due_date_time,
                        end: item.publishing_date_time,
                    })
                ))

            ))

            setSchedule(schedule)
        }

        filterSchedule()

    }, [project.project_id])


    const onDateClick = ({ dateStr }) => {
        setOpen(true)
        setProject({
            ...project,
            due_date: dateStr
        })
    }

    return (
        <>
            <div className='project-calendar'>
                <FullCalendar
                    plugins={[dayGridPlugin, interactionPlugin]}
                    initialView="dayGridMonth"
                    dateClick={onDateClick}
                    events={schedule}
                    headerToolbar={{
                        left: 'title prev next dayGridMonth today',
                        right: ''
                    }}
                    fixedWeekCount={false}
                    height={700}
                    dayMaxEventRows={4}
                />
            </div>

            <CreateContent
                openDrawer={open}
                onClose={() => setOpen(false)}
            />
        </>
    )
}

export default ProjectCalendar